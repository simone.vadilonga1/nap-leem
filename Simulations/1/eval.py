import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# path to xrt:
import os, sys; sys.path.append(os.path.join('..', '..', '..'))  # analysis:ignore
import xrt.backends.raycing.materials as rm

# raypyng 
from raypyng.postprocessing import PostProcessAnalyzed

# from helper library
from helper_lib import get_reflectivity
from helper_lib import scale_undulator_flux, order

# import simulation parameters
from parameter import order, energy_rp, energy_flux
from parameter import SlitSize

SlitSize = SlitSize*1000


# colors for plotting
colors = ['r', 'g', 'b']

ppa = PostProcessAnalyzed()


# file/folder/ml index definition
flux_simulation_folder = 'RAYPy_Simulation_NAP-LEEM_FLUX'
rp_simulation_folder = 'RAYPy_Simulation_NAP-LEEM_RP'
# load Flux simulations results
folder_name  = flux_simulation_folder
source       = 'SU'
oe           = 'DetectorAtFocus'
nsim         = energy_flux.shape[0]*SlitSize.shape[0]

from flux import retrieve_flux_beamline
flux = ppa.retrieve_flux_beamline(folder_name, source, oe, nsim, rounds=1, current=0.3)

# load RP simulations results
rounds      = 2
folder_name = rp_simulation_folder
oe          = 'DetectorAtFocus'
nsim        = energy_rp.shape[0]*SlitSize.shape[0]

bw, focx, focy = ppa.retrieve_bw_and_focusSize(folder_name, oe, nsim, rounds=rounds)
focx = focx*1000 # in um
focy = focy*1000 # in um

################################################################
## KB

# file/folder/ml index definition
flux_simulation_folder = 'RAYPy_Simulation_NAP-LEEM-KB_FLUX'
rp_simulation_folder = 'RAYPy_Simulation_NAP-LEEM-KB_RP'
# load Flux simulations results
folder_name  = flux_simulation_folder
source       = 'SU'
oe           = 'DetectorAtFocus'
nsim         = energy_flux.shape[0]*SlitSize.shape[0]

from flux import retrieve_flux_beamline
kb_flux = ppa.retrieve_flux_beamline(folder_name, source, oe, nsim, rounds=1, current=0.3)

# load RP simulations results
rounds      = 2
folder_name = rp_simulation_folder
oe          = 'DetectorAtFocus'
nsim        = energy_rp.shape[0]*SlitSize.shape[0]

kb_bw, kb_focx, kb_focy = ppa.retrieve_bw_and_focusSize(folder_name, oe, nsim, rounds=rounds)
kb_focx = kb_focx*1000 # in um
kb_focy = kb_focy*1000 # in um




########################################
# plotting Flux and RP

fig, (axs) = plt.subplots(3, 2,figsize=(10,10))
fig.suptitle("NAP-LEEM, 1200 l/mm grating")


# MIRROR COATING
# de = 38.9579-30.0000
# table = 'Henke'
# theta = 
# E = np.arange(50, 5001, de)
# Ir  = rm.Material('Ir',  rho=22.56, kind='mirror',table=table)
# Cr  = rm.Material('Cr',  rho=7.15,  kind='mirror',table=table)
# B4C = rm.Material('C', rho=2.52,  kind='mirror',  table=table)
# IrCrB4C = rm.Multilayer( tLayer=B4C, tThickness=40, 
#                         bLayer=Cr, bThickness=60, 
#                         nPairs=1, substrate=Ir)

# IrCrB4C, _ = get_reflectivity(IrCrB4C, E=E, theta=theta)

# ax2=axs[0,0]
# ax2.set_xlabel('Energy [eV]')
# ax2.set_ylabel('Reflectivity [a.u.]')
# ax2.set_title('Mirror Coating Reflectivity')
# ax2.plot(E, IrCrB4C, 'b', label='IrCrB4C')
# ax2.legend()



# BEAMLINE TRANSMISSION
ax = axs[0,1]

ss = energy_flux.shape[0]

for ind, es in enumerate(SlitSize):
    ax.plot(energy_flux,flux[ss*ind:ss*(ind+1)], colors[ind], label=f'ExitSlit {es} um' )

for ind, es in enumerate(SlitSize):
    ax.plot(energy_flux,kb_flux[ss*ind:ss*(ind+1)], colors[ind],linestyle='dashed', label=f'KB ExitSlit {es} um' )



ax.set_xlabel(r'Energy [eV]')
ax.set_ylabel('Transmission [%]')
ax.set_title('Available Flux (in transmitted bandwidth)')
ax.grid(which='both', axis='both')
ax.legend()



# BANDWIDTH
ax = axs[1,0]
for ind, es in enumerate(SlitSize):
    ax.plot(energy_rp,bw[ss*ind:ss*(ind+1)],colors[ind],label=f'ExitSlit {es} um')

for ind, es in enumerate(SlitSize):
    ax.plot(energy_rp,kb_bw[ss*ind:ss*(ind+1)],colors[ind],linestyle='dashed',label=f'KB ExitSlit {es} um')

ax.set_xlabel('Energy [eV]')
ax.set_ylabel('Transmitted Bandwidth [eV]')
ax.set_title('Transmitted bandwidth (tbw)')
ax.grid(which='both', axis='both')
# ax.legend()


# RESOLVING POWER
ax = axs[1,1]

# plot and deal with bandwidth=0 case.
for ind, es in enumerate(SlitSize):
    try:
        rp = energy_rp / bw[ss * ind:ss * (ind + 1)]
    except ZeroDivisionError:
        rp = 0
    inf_indices = np.where(np.isinf(rp))[0]
    if len(inf_indices)>0:
        print(f"For ExitSlit size {ss}, you have zero bandwidth starting at E={energy_rp[inf_indices[0]]} eV.")
    ax.plot(energy_rp, rp, colors[ind], label=f'ExitSlit {es} um')


# plot and deal with bandwidth=0 case.
for ind, es in enumerate(SlitSize):
    try:
        kb_rp = energy_rp / kb_bw[ss * ind:ss * (ind + 1)]
    except ZeroDivisionError:
        kb_rp = 0
    inf_indices = np.where(np.isinf(kb_rp))[0]
    if len(inf_indices)>0:
        print(f"For KB ExitSlit size {ss}, you have zero bandwidth starting at E={energy_rp[inf_indices[0]]} eV.")
    ax.plot(energy_rp, kb_rp, colors[ind],linestyle='dashed', label=f'KB ExitSlit {es} um')

ax.set_xlabel('Energy [eV]')
ax.set_ylabel('RP [a.u.]')
ax.set_title('Resolving Power')
ax.grid(which='both', axis='both')
ax.legend()

# HORIZONTAL FOCUS
ax = axs[2,0]
for ind, es in enumerate(SlitSize):
    ax.plot(energy_rp,focx[ss*ind:ss*(ind+1)],colors[ind],label=f'ExitSlit {es} um')

for ind, es in enumerate(SlitSize):
    ax.plot(energy_rp,kb_focx[ss*ind:ss*(ind+1)],colors[ind],linestyle='dashed',label=f'KB ExitSlit {es} um')

ax.set_xlabel('Energy [eV]')
ax.set_ylabel('Focus Size [um]')
ax.set_title('Horizontal focus')
ax.legend()

# # VERTICAL FOCUS
ax = axs[2,1]
for ind, es in enumerate(SlitSize):
    ax.plot(energy_rp,focy[ss*ind:ss*(ind+1)],colors[ind],label=f'ExitSlit {es} um')

for ind, es in enumerate(SlitSize):
    ax.plot(energy_rp,kb_focy[ss*ind:ss*(ind+1)],colors[ind],linestyle='dashed',label=f'KB ExitSlit {es} um')

ax.set_xlabel('Energy [eV]')
ax.set_ylabel('Focus Size [um]')
ax.set_title('Vertical focus')

plt.tight_layout()
plt.savefig('plot/FluxRpFocus.png')

# plt.show()