import numpy as np

order       = 1
SlitSize    = np.array([0.05])
grating     = np.array([1200])
blaze       = np.array([0.9])
cff         = np.array([2.25])

energy_flux = np.arange(200, 2201,200)
energy_rp   = np.arange(200, 2201,500)

nrays_flux  = 5e4 
nrays_rp    = 5e5

short_radius_m3 = np.arange(100,180, 5)