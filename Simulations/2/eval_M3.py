import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# path to xrt:
import os, sys; sys.path.append(os.path.join('..', '..', '..'))  # analysis:ignore
import xrt.backends.raycing.materials as rm

# raypyng 
from raypyng.postprocessing import PostProcessAnalyzed

# from helper library
from helper_lib import get_reflectivity
from helper_lib import scale_undulator_flux, order

# import simulation parameters
from parameter import SlitSize
from parameter import short_radius_m3


# file/folder/ml index definition
rp_simulation_folder = 'RAYPy_Simulation_NAP-LEEM_M3'

# colors for plotting
colors = ['r', 'g', 'b']

ppa = PostProcessAnalyzed()

# load RP simulations results
rounds      = 2
folder_name = rp_simulation_folder
oe          = 'DetectorAtExitSlit'
nsim        = short_radius_m3.shape[0]

bw, focx, focy = ppa.retrieve_bw_and_focusSize(folder_name, oe, nsim, rounds=rounds)
focx = focx*1000 # in um
focy = focy*1000 # in um




########################################
# plotting Flux and RP

fig, (axs) = plt.subplots(1, 1,figsize=(10,10))
fig.suptitle("NAP-LEEM, M3 tuning")


# BEAMLINE TRANSMISSION



axs.plot(short_radius_m3,focy, colors[0])

axs.set_xlabel(r'short_radius_m3 [mm]')
axs.set_ylabel('Transmission [%]')
axs.set_title('Available Flux (in transmitted bandwidth)')
axs.grid(which='both', axis='both')



plt.tight_layout()
plt.savefig('plot/M3_tuning.png')

# plt.show()